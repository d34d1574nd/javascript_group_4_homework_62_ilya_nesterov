import React, {Component, Fragment} from 'react';
import {NavLink} from "react-router-dom";

import './Contacts.css';

class Contacts extends Component {
  render() {
    return (
      <Fragment>
        <div className='homepage'>
          <NavLink to='/'>My Company</NavLink>
          <NavLink to="/">Home</NavLink>
          <NavLink to="/about">About</NavLink>
          <NavLink to="/contacts">Contacts</NavLink>
          <NavLink to="/discounts">Discounts</NavLink>
        </div>

        <div className='contacts'>
          <div className="aboutus">
            <strong>НАШ АДРЕС:</strong>
            <p>г. Бишкек, бульвар Молодая Гвардия 20 (западная сторона) Между ул. Токтогула и ул. Московской</p>
            <hr/>
            <strong>НАШИ ТЕЛЕФОНЫ:</strong>
            <p>Тел.: +996312652823 +996553918543 +996559918543</p>
            <hr/>
            <strong>РЕЖИМ РАБОТЫ:</strong>
            <p>с 9:30 до 18:00 (без перерыва)</p>
            <p>Суббота с 9:30 до 14:00</p>
            <p>Воскресенье - выходной</p>
          </div>
          <div className="contact">
            <strong>КОНТАКТЫ:</strong>
            <strong>Skype:</strong> <p>notnik.com (только чат)</p>
            <strong>Аgent:</strong> <p>notnik.com@mail.ru (только чат)</p>
            <strong>ICQ:</strong> <p>302222234</p>
            <strong>WhatsApp:</strong> <p>+996553918543 (только чат)</p>
          </div>
        </div>
      </Fragment>
    );
  }
}

export default Contacts;