import React, {Component, Fragment} from 'react';
import {NavLink} from "react-router-dom";

import './Discounts.css'

class Profile extends Component {
  render() {
    return (
      <Fragment>
        <div className='homepage'>
          <NavLink to='/'>My Company</NavLink>
          <NavLink to="/">Home</NavLink>
          <NavLink to="/about">About</NavLink>
          <NavLink to="/contacts">Contacts</NavLink>
          <NavLink to="/discounts">Discounts</NavLink>
        </div>
        <div className="price">
          <div className='discounts'>
            <div className='section'>
              <span>Специально для Вас мы разработали систему скидок по дням недели. С понедельника по пятницу.
                Вы сможете экономить до 50% от реальной стоимости услуги.
                Мы выбрали самые типичные поломки ноутбуков.</span>
            </div>
          </div>
          <div className="discount">
            <span>СКИДКИ</span>
            <p><span>Пятница:</span> 50% Чистка системы охлаждения с заменой термоинтерфейсов</p>
            <p><span>Четверг:</span> 40% Замена любого разьёма (Power Jack, USB, HDMI, Sound, и тп)</p>
            <p><span>Среда:</span> 30% Замена кулера или радиатора (Система охлаждения)</p>
            <p><span>Вторник:</span> 20% Восстановление корпуса ноутбука (петли и внутренние крепления)</p>
            <p><span>Понедельник:</span> 10% Реболлинг BGA (графический чип, северный мост, южный мост, любой BGA чип)</p>
          </div>
          <p>Правила акций и скидок:</p>
          <p>*Вы должны знать о них, либо с нашего сайта, либо с других источников
            (обязательно говорите менеджеру при приёме ноутбука на ремонт!)  Иначе скидок не будет.</p>
        </div>
      </Fragment>
    );
  }
}

export default Profile;