import React, {Component, Fragment} from 'react';
import {NavLink} from "react-router-dom";

import './AboutUs.css';

class AboutUs extends Component {
  render() {
    return (
      <Fragment>
        <div className='homepage'>
          <NavLink to='/'>My Company</NavLink>
          <NavLink to="/">Home</NavLink>
          <NavLink to="/about">About</NavLink>
          <NavLink to="/contacts">Contacts</NavLink>
          <NavLink to="/discounts">Discounts</NavLink>
        </div>
        <div className='about'>
            <p>СЛОМАЛСЯ НОУТБУК?</p>
            <ol>
              <li>Мы ремонтируем ноутбуки с 2001 года.</li>
              <li>Мы первая и единственная в городе Бишкек мастерская, которая  занимается только ремонтом ноутбуков.</li>
              <li>Мы имеем огромную базу запчастей  и высокотехнологичное оборудование для ремонта и диагностики.</li>
              <li>Наш сайт создан для ценителей своего времени. Здесь не будет скучного набора букв... Всё просто.</li>
            </ol>
        </div>
        <div className="footer">
          <div className="accounting">
            <strong>82,71% (16.01.2019)</strong>
            <p>Что это такое? Это статистика  положительных ремонтов, выполненных в нашей мастерской.
              Именно ремонт, а не замена комплектующих. Статистика начинается с 2017 года и обновляется примерно один раз в месяц.
            </p>
          </div>
        </div>
      </Fragment>
    );
  }
}

export default AboutUs;