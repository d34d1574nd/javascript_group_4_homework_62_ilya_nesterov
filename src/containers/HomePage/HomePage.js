import React, {Component, Fragment} from 'react';
import {NavLink} from 'react-router-dom';

import './HomePage.css'

class HomePage extends Component {
  render() {
    return (
      <Fragment>
        <div className='homepage'>
          <NavLink to='/'>My Company</NavLink>
          <NavLink to="/">Home</NavLink>
          <NavLink to="/about">About</NavLink>
          <NavLink to="/contacts">Contacts</NavLink>
          <NavLink to="/discounts">Discounts</NavLink>
        </div>
        <div className='home'>
          <div className="page">
            <span className='attention'>Очень важно! Стоит ли платить за диагностику?</span>
            <hr/>
            <p>Типичная ситуация. На экране ноутбука нет изображения и даже подсветки.
              Клиент  на 100% уверен что это шлейф (обычно, ему сказал это знакомый супер мастер или гугл ).
              Меняем шлейф и результата нет. Потом клиент  возмущается что не отремонтировали его нотник...</p>
            <hr/>
            <p>Уважаемые клиенты, симптомы поломок очень похожи
              и никогда нельзя сказать со 100%   уверенностью что именно
              вышло из строя,  для этого и существует диагностика.
              Ремонт без дифектовки деталей невозможен!!!</p>
            <hr/>
            <p>Диагностика оборудования в нашей мастерской от 200с до 600с, зависит от срочности.</p>
          </div>
        </div>
      </Fragment>
    );
  }
}

export default HomePage;