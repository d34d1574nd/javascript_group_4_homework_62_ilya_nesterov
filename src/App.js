import React, { Component } from 'react';
import './App.css';
import {BrowserRouter, Switch, Route} from "react-router-dom";
import HomePage from "./containers/HomePage/HomePage";
import AboutUs from "./components/AboutUs/AboutUs";
import Contacts from "./components/Contacts/Contacts";
import Discounts from "./components/Discounts/Discounts";


class App extends Component {
  render() {
    return (
      <BrowserRouter>
        <Switch>
          <Route path="/" exact component={HomePage} />
          <Route path="/about" component={AboutUs} />
          <Route path="/contacts" component={Contacts} />
          <Route path="/discounts" component={Discounts} />
        </Switch>
      </BrowserRouter>
    );
  }
}

export default App;
